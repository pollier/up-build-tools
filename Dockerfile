FROM        hashicorp/terraform AS terraform
FROM        ubuntu:focal
SHELL       ["/bin/bash", "-o", "pipefail", "-c"]

ENV         DEBIAN_FRONTEND=noninteractive
ENV         TZ=UTC

RUN         apt-get update && apt-get upgrade -y \
            && apt-get install -y \
            apt-transport-https \
            ca-certificates \
            curl \
            docker.io \
            gettext \
            git \
            gnupg-agent \
            httpie \
            lbzip2 \
            openssl \
            pigz \
            python3-pip \
            shellcheck \
            software-properties-common \
            unzip \
            wget \
            yamllint \
            && pip3 install j2cli lastversion Pygments

RUN         curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN         curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN         echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN         apt-get update && apt-get install -y nodejs yarn

WORKDIR     /bin

COPY        --from=terraform /bin/terraform /bin/terraform
RUN         chmod +x /bin/terraform
RUN         terraform --version

RUN         curl -LO https://storage.googleapis.com/kubernetes-release/release/"$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)"/bin/linux/amd64/kubectl
RUN         chmod +x kubectl

RUN         curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

RUN         curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
            && unzip awscliv2.zip \
            && ./aws/install
RUN         aws --version
RUN         curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
RUN         mv /tmp/eksctl /bin
RUN         chmod +x /bin/eksctl
